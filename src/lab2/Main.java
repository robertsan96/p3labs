package lab2;

public class Main {

	public static void main(String[] args) {		
		/**
		 * 1. Create an array of a of n random generated numbers. The dimension of the array is passed like
		 * argument on command line. Resolve the following requests:
		 *	a) Display the array
		 *  b) Sort the array and display the sorted array
		 *  c) Copy a subarray of the array a in a new array, the start and stop indexes for coping are 
		 *  random generated
		 */
//		ExerciseOne exerciseOne = new ExerciseOne(args); 
		
		/**
		 * 2. Get a sentence like an argument from command line and resolve the following requests:
		 *  a) Find how many words are in the sentence. A word can be separated by one ore multiple
		 *  spaces or tabs.
		 *  b) Count the numbers of palindrome words from the sentence
		 *  c) Display the last 10 characters from the sentence.
		 *  d) Transform the sentence to uppercase and lowercase.
		 *  e) Find if a substring is present in the sentence.
 		 *  f) Convert the sentence based on the following rule each vocal is replaced with vocal’p’vocal.
		 * Ex: i -> ipi, a->api
		 */
		ExerciseTwo exerciseTwo = new ExerciseTwo(args);
	}
	
	

}
