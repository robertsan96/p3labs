package lab2;

import java.util.Arrays;
import java.util.Random;

/**
 * 1. Create an array of a of n random generated numbers. The dimension of the array is passed like
 * argument on command line. Resolve the following requests:
 *	a) Display the array
 *  b) Sort the array and display the sorted array
 *  c) Copy a subarray of the array a in a new array, the start and stop indexes for coping are 
 *  random generated
 */

public class ExerciseOne {
	
	public int[] array;
	public int[] subArray;
	
	public ExerciseOne(String[] args) {
		int arraySize = Integer.parseInt(args[0]);
		array = buildArray(arraySize);

		System.out.println("ARRAY: ");
		displayArray(array);
		
		Arrays.sort(array);
		
		System.out.println("SORTED ARRAY: ");
		displayArray(array);
		
		Random random = new Random();
		int minRange = random.nextInt(arraySize/2)+0;
		int maxRange = random.nextInt(arraySize)+arraySize/2-1;
		subArray = buildSubArrayFromArrayInRange(array, minRange, maxRange);
		
		System.out.println("SUBARRAY: ");
		displayArray(subArray);
	}
	
	public int[] buildArray(int arraySize) {
		int[] builtArray = new int[arraySize];
		for(int i=0; i<arraySize; i++) {
			int randomArrayValue = new Random().nextInt(500)+1;
			builtArray[i] = randomArrayValue;
		}
		return builtArray;
	}
	
	public void displayArray(int[] array) {
		for(int i=0; i<array.length; i++) {
			System.out.println(array[i] + " ");
		}
	}
	
	public int[] buildSubArrayFromArrayInRange(int[] array, int minRange, int maxRange) {
		int[] subArray = new int[maxRange-minRange];
		int index = 0;
		for(int i=minRange; i<maxRange; i++) {
			subArray[index] = array[i];
			index += 1;
		}
		return subArray;
	}
}
