package lab2;

import java.util.ArrayList;

/**
 * 2. Get a sentence like an argument from command line and resolve the following requests:
 *  a) Find how many words are in the sentence. A word can be separated by one ore multiple
 *  spaces or tabs.
 *  b) Count the numbers of palindrome words from the sentence
 *  c) Display the last 10 characters from the sentence.
 *  d) Transform the sentence to uppercase and lowercase.
 *  e) Find if a substring is present in the sentence.
 *  f) Convert the sentence based on the following rule each vocal is replaced with vocal’p’vocal.
 * Ex: i -> ipi, a->api
 */

public class ExerciseTwo {

	public ExerciseTwo(String[] sentence) {
		this.countWords(sentence);
		this.countPalindromeWords(sentence);
		this.displayLastCharacters(sentence, 10);
		String sentenceAsString = this.stringArrayToString(sentence);
		System.out.println("Lowercased: " + sentenceAsString.toLowerCase());
		System.out.println("Uppercased: " + sentenceAsString.toUpperCase());
		CharSequence wordToSearchFor = "fox";
		System.out.println("Sentence contains '"+ wordToSearchFor + "': " + sentenceAsString.contains(wordToSearchFor));
		sentenceAsString.contains("test");
	}
	
	public void countWords(String[] sentence) {
		System.out.println("There are " + sentence.length + " words in the sentence.");
	}
	
	public void countPalindromeWords(String[] sentence) {
		int palindromeWordsCounter = 0;
		for(String word : sentence) {
			System.out.println("isTheWordPalindrome(" +word + "): " + this.isTheWordPalindrome(word));
			if(this.isTheWordPalindrome(word)) {
				palindromeWordsCounter += 1;
			}
		}
		System.out.println("Palindrome words: " + palindromeWordsCounter);
	}
	
	public Boolean isTheWordPalindrome(String word) {
		word = word.toLowerCase();
		int wordLength = word.length();
		boolean isPalindrome = true;
		
		for(int startPointer=0; startPointer<wordLength; startPointer++) {
			char startChar = word.charAt(startPointer);
			char endChar = word.charAt((wordLength-1)-startPointer);
			if(startChar != endChar) {
				isPalindrome = false;
			}
		}
		return isPalindrome;
	}
	
	public void displayLastCharacters(String[] sentence, int charactersCounter) {
		String sentenceAsString = this.stringArrayToString(sentence);
		
		int sentenceLength = sentenceAsString.length();
		if(sentenceLength < charactersCounter) {
			System.out.println("You can't show more characters than the sentence has. (sentence: "+ sentenceLength +")");
		} else {
			ArrayList<String> lastCharacters = new ArrayList<String>();
			for(int endPointer = sentenceLength-1; endPointer>=sentenceLength-charactersCounter; endPointer--) {
				char character = sentenceAsString.charAt(endPointer);
				lastCharacters.add(String.valueOf(character));
			}
			System.out.println(lastCharacters);	
		}
	}
	
	public String stringArrayToString(String[] array) {
		String string = new String();
		for(String word : array) {
			string = string.concat(word + " ");
		}
		return string;
	}
	
}













