package lab1;

import java.util.ArrayList;
import java.util.Random;

public class Main {
	
	public static void main(String[] args) {
		Main main = new Main();
		/**
		 *  Display to standard output the message “Hello word!”
		 */
//		main.displayHelloWorld();

		/**
		 *  Display to standard output all parameters passed on command line of a java program.
		 */
//		main.displayArguments(args);
		/**
		 *  Calculate the biggest common divisor of two numbers a and b that are 
		 *  passed like arguments on command line.
		 */
//		main.displayBiggestCommonDivisorFromArgs(args);
		/**
		 * Simulate the dice throwing action, for a n numbers of times (n is passed
		 * like argument on command line) using the random number generator from:
		 *	a) Math class
		 *	b) Random class
		 */
//		main.simulateDiceForNumberOfTimes(Integer.parseInt(args[0]));
		/**
		 * Calculate n! for a number n that is passed like argument on command line.
		 * The factorial will be calculated only for numbers n that satisfies the
		 * following condition: 0 <= n <= 12		
		 */
//		main.computeFactorial(Integer.parseInt(args[0]));
		/**
		 * Write a program that verifies if a number passed like command line
		 * argument is a palindrome. 
		 */
//		main.isTheNumberPalindrome(Integer.parseInt(args[0]));
		/**
		 * Write a program that displays at standard output the first n terms of
	     * Fibonacci sequence:
		 * a0 = a1 = 0, an = an-1 + an-2
		 * The number n is passed like argument on command line.
		 */
//		main.computeFibonacci(Integer.parseInt(args[0]));
		/**
		 * Write a program that checks if a number n random generated, with the 
		 * property that n<300 is prime.
		 */
		main.isTheNumberPrime(new Random().nextInt(300)+1);
	}
	
	public void displayHelloWorld() {
		System.out.println("Hello world");	
	}
	
	public void displayArguments(String[] args) {
		for(String arg : args) {
			System.out.println(arg);
		}
	}
	
	public void displayBiggestCommonDivisorFromArgs(String[] args) {
		int numberOne = Integer.parseInt(args[0]);
		int numberTwo = Integer.parseInt(args[1]);
		
		int biggestNumber = (numberOne >= numberTwo) ? numberOne : numberTwo;
		int biggestCommonDivisor = 1;
		
		for(int i=2; i<=biggestNumber; i++) {
			if(numberOne%i == 0 && numberTwo%i == 0) {
				biggestCommonDivisor = i;
			}
		}
		System.out.println("The biggest common divisor "
				+ "between " + numberOne + " and " + numberTwo + 
				" is " + biggestCommonDivisor);
	}
	
	public void simulateDiceForNumberOfTimes(int numberOfTimes) {
		Random random = new Random();
		ArrayList<Integer> diceValuesByRandom = new ArrayList<Integer>();
		ArrayList<Integer> diceValuesByMathRandom = new ArrayList<Integer>();
		
		for(int i=0; i<numberOfTimes; i++) {
			int diceValueByRandom = random.nextInt(6) + 1;
			int diceValueByMathRandom = this.randomWithRange(1, 6);
			diceValuesByRandom.add(Integer.valueOf(diceValueByRandom));
			diceValuesByMathRandom.add(Integer.valueOf(diceValueByMathRandom));
		}
		System.out.println("Dice values by Random(): " + diceValuesByRandom);
		System.out.println("Dice values by Math.random(): " + diceValuesByMathRandom);
	}
	
	public int randomWithRange(int min, int max) {
		int range = (max - min) + 1;     
		return (int)(Math.random() * range) + min;
	}
	
	public void computeFactorial(int ofNumber) {
		Boolean passedCondition = ((ofNumber >= 0) && (ofNumber <= 12)) ? true : false;
		if(passedCondition) {
			// If @ofNumber is less than 1
			if (ofNumber < 1) {
				System.out.println("Result: 0.");
			} else {
				long factorialResult = 1;
				for(int i=1; i<=ofNumber; i++) {
					factorialResult = factorialResult * i;
				}
				System.out.println("Result: " + factorialResult + ".");
			}
		} else {
			System.out.println("The integer must be in the [0, 12] interval.");
		}
	}
	
	public void isTheNumberPalindrome(int number) {
		ArrayList<Integer> individualCiphersArray = new ArrayList<Integer>();
		while(number != 0) {
			int lastCipher = number%10;
			number = number/10;
			individualCiphersArray.add(Integer.valueOf(lastCipher));
		}
		System.out.println(individualCiphersArray);
		int steps = individualCiphersArray.size() / 2;
		boolean isPalindrome = true;
		for(int startPointer=0; startPointer<steps; startPointer++) {
			int leftCipher = individualCiphersArray.get(startPointer);
			int rightCipher = individualCiphersArray.get((individualCiphersArray.size() - 1) - startPointer);
			if (leftCipher != rightCipher) {
				isPalindrome = false;
				break;
			}
		}
		if(isPalindrome) {
			System.out.println("The number IS PALINDROME.");
		} else {
			System.out.println("The number IS NOT PALINDROME.");
		}
	}
	
	public void computeFibonacci(int firstTerms) {
		ArrayList<Integer> fibonacciSeries = new ArrayList<Integer>();
		// Seed values (so we have the first two terms)
		// Either [0, 1] or [1, 1]
		fibonacciSeries.add(Integer.valueOf(1));
		fibonacciSeries.add(Integer.valueOf(1));
		
		if (firstTerms > 2) {
			int index = 2; // as we already have 2 items. [0, 1, hereWeGo];
			while(firstTerms != fibonacciSeries.size()) {
				int nextFibonacciNumber = fibonacciSeries.get(index-1) + fibonacciSeries.get(index-2);
				fibonacciSeries.add(Integer.valueOf(nextFibonacciNumber));
				index += 1;
			}
			System.out.println("Fibonacci series: " + fibonacciSeries);
		} else {
			System.out.println("Fibonacci series: " + fibonacciSeries);
		}
	}
	
	public void isTheNumberPrime(int theNumber) {
		Boolean isPrime = true;
		for(int i=2; i<=theNumber; i++) {
			if(i != theNumber) {
				if(theNumber%i == 0) {
					isPrime = false;
				}	
			}
		}
		if(isPrime) {
			System.out.println(theNumber + " is prime.");
		} else {
			System.out.println(theNumber + " is not prime.");
		}
	}

}
